# Aislinn - Fabric - 1.20.1

Last Updated: 4/5/2024

## Installation

> Note: Install Minecraft normally before the steps below.

1. Download the [Fabric Installer](https://fabricmc.net/use/installer/)
2. Install as the `Client` with the following:
   > ![fabric-installer-guide](./img/fabric-installer-guide.png)
3. Click `mods` on the prompt post-installation or navigate to your `mods` folder for the profile
   > ![post-install-prompt](./img/post-install-prompt.png)
4. Paste the contents of this repository's `mods` and `client` folder into the `mods` folder of the new profile
5. Back up one directory in the folder and paste the `config` folder
6. Open `Minecraft` and edit the profile's advanced options with the memory allocation of your choice:
   > ![](./img/edit-profile.mov)
7. Save your options, select the profile, and hit `Play`
8. Connect to the server once, allow configurations to sync, allow the client to restart
   > ![config-sync](./img/config-sync.jpg)
9. Start up the game once more, connect again, and the rest is history

## Server Details

```
Connection IP: aislinn.duckdns.org:25565
```

## Beautify your Minecraft

- There are 2 ways to beautify your MC.

#### 1. Use Distant Horizons With or Without Shaders

- From testing, this has not affected FPS much.
- It's in beta, and it shows. There are some interesting artifacting when looking out in the distance.
- You limit your options on shader variation if you choose to use it. I believe only `Bliss` and `Complementary` work.

#### 2. Ignore Distant Horizons and use shaders like in the older servers

- Technically better performance.
- Less buggy visuals when looking out in the distance.

I have been experimenting with option 1, so if you want to also use option 1, here are a few small steps:

1. Go to the graphics options -> shaders -> enable Bliss shaders. I have pushed my configs for Bliss too, so it should basically work out of the box. 
2. Click on the `Distant Horizons` config menu within the options at the top left: 
![DH Icon](./img/DH%20Icon.png)
3. Make sure the render options are enabled. The default render distance is 128. I pushed mine to 256, but 128 is probably fine lol.

If you want to play the game without `Distant Horizons`:

1. Click on the `Distant Horizons` option that you can find using the picture above. 
2. Ensure that everything regarding rendering on the main config page is disabled.
3. Go to the shader menu if you want to play with shaders. Now that Distant Horizon is disabled, you can basically pick any of them and mess around with the options.

#### Customizing the look of mobs and blocks

There are a few options to configure here as well based on what you prioritize:

**I remember certain people wanting a... caste system of villagers based on appearance. If you want that...**

Ensure that `mizuno` resource packs are ABOVE `Fresh animations`. There are a few cons tho:

1. Mizuno author is not using proper pathing for `EMF`, so the faces have no depth on villagers. 
2. Some mob like sheep and cows might look buggy due to this pathing.

**If you want everything to look... anatomically correct and also have updated animations...**

1. Ensure that `Fresh Animations` is above any block texture pack like `mizuno`.
2. Ensure that `Better Dogs` and `Better Cats` are **above** fresh animations

Everything else can be loaded in any basically any order.

Here's an example loadout that **prioritizes the caste system** lol:

![resource pack order](./img/resourcepack-order.png)

The `DefaultPBR` pack is mainly for shaders. It introduces normal mapping on blocks using the vanilla MC look.

#### What is Better Dogs and Better Cats?

These resource packs introduce different dog and cat species. However, the entities become somewhat CPU intensive. Can disable it if your game is chugging too much. 

With the `Better_Dogs_X_Doggy_Talents_Next_v1.2.1` mod, **Adding the Better Dogs resource pack will MAKE WOLVES INVISIBLE**. That resourcepack is there as a backup just in case the mod does not do what it is intending to do. 